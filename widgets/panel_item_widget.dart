import 'package:flutter/material.dart';
import 'package:budget_test/models/panel_item.dart';
import 'package:budget_test/models/product_item.dart';
import 'package:budget_test/widgets/product_item_widget.dart';

class PanelItemWidget extends StatefulWidget {
  final PanelItem budget;
  final ValueChanged<bool> onExpansionChange;
  final VoidCallback onAddSubItem;
  final ValueChanged<int> onEditSubItem;

  PanelItemWidget({
    required this.budget,
    required this.onExpansionChange,
    required this.onAddSubItem,
    required this.onEditSubItem,
  });

  @override
  _PanelItemWidgetState createState() => _PanelItemWidgetState();
}

class _PanelItemWidgetState extends State<PanelItemWidget> with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 2);  // 2 tabs
  }

  @override
  Widget build(BuildContext context) {
    return ExpansionPanelList(
      expansionCallback: (int index, bool isExpanded) {
        widget.onExpansionChange(!isExpanded);
      },
      children: [
        ExpansionPanel(
          headerBuilder: (BuildContext context, bool isExpanded) {
            return ListTile(
              title: Text(widget.budget.headerValue),
              subtitle: Text(
                  'Planned: ${widget.budget.plannedExpenses}, Current: ${widget.budget.currentExpenses}'),
              trailing: IconButton( // новая кнопка для добавления подпункта
                icon: Icon(Icons.add),
                onPressed: widget.onAddSubItem,
              ),
            );
          },
          body: Column(
            children: [
              // Здесь отображаются подэлементы budget
              for (var index = 0; index < widget.budget.subItems.length; index++)
                ProductItemWidget(
                  productItem: widget.budget.subItems[index],
                  onEdit: () => widget.onEditSubItem(index),
                ),
              ElevatedButton(
                child: Text('Добавить'),
                onPressed: widget.onAddSubItem, // Это вызовет метод onAddSubItem в HomeScreen
              ),
            ],
          ),
          isExpanded: widget.budget.isExpanded,
        ),
      ],
    );
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }
}
