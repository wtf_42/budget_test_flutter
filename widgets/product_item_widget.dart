import 'package:flutter/material.dart';
import 'package:budget_test/models/product_item.dart';
import 'package:budget_test/screens/edit_product_item_screen.dart';

class ProductItemWidget extends StatelessWidget {
  final ProductItem productItem;
  final VoidCallback onEdit;

  ProductItemWidget({
    required this.productItem,
    required this.onEdit
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(productItem.headerValue),
      subtitle: Text('Planned: ${productItem.plannedExpenses}, Current: ${productItem.currentExpenses}'),
      trailing: IconButton(
        icon: Icon(Icons.edit),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => EditProductItemScreen(productItem: productItem),
            ),
          ).then((updatedProductItem) {
            if (updatedProductItem != null) {
              // update the productItem with the updatedProductItem
              // You will need to handle this part to update the productItem in your list
            }
          });
        },
      ),
    );
  }
}
