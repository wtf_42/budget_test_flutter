import 'package:flutter/material.dart';
import 'package:budget_test/models/panel_item.dart';
import 'package:budget_test/models/product_item.dart';
import 'package:budget_test/screens/add_product_item_screen.dart';

class AddPanelItemScreen extends StatefulWidget {
  @override
  _AddPanelItemScreenState createState() => _AddPanelItemScreenState();
}

class _AddPanelItemScreenState extends State<AddPanelItemScreen> {
  final _formKey = GlobalKey<FormState>();
  String _headerValue = '';
  List<ProductItem> _subItems = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Item'),
      ),
      body: Form(
        key: _formKey,
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              TextFormField(
                decoration: InputDecoration(labelText: 'Header Value'),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter a header value';
                  }
                  return null;
                },
                onSaved: (value) {
                  _headerValue = value!;
                },
              ),
              ElevatedButton(
                onPressed: _saveForm,
                child: Text('Save'),
              ),
              // ElevatedButton(
              //   onPressed: _addSubItem,
              //   child: Text('Add SubItem'),
              // ),
            ],
          ),
        ),
      ),
    );
  }

  void _saveForm() {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      double plannedExpenses = _subItems.fold(0, (prev, item) => prev + item.plannedExpenses);
      double currentExpenses = _subItems.fold(0, (prev, item) => prev + item.currentExpenses);
      Navigator.pop(context, PanelItem(
        headerValue: _headerValue,
        plannedExpenses: plannedExpenses,
        currentExpenses: currentExpenses,
        isExpanded: false,
        expandedValue: 'Test',
        subItems: _subItems,
      ));
    }
  }

  // void _addSubItem() async {
  //   final newSubItem = await Navigator.push(context, MaterialPageRoute(builder: (context) => AddSubItemScreen()));
  //   if (newSubItem != null) {
  //     setState(() {
  //       _subItems.add(newSubItem);
  //     });
  //   }
  // }
}