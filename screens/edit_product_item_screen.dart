import 'package:flutter/material.dart';
import 'package:budget_test/models/product_item.dart';

class EditProductItemScreen extends StatefulWidget {
  final ProductItem productItem;

  EditProductItemScreen({required this.productItem});

  @override
  _EditProductItemScreenState createState() => _EditProductItemScreenState();
}

class _EditProductItemScreenState extends State<EditProductItemScreen> {
  final _formKey = GlobalKey<FormState>();
  String _headerValue = '';
  double _plannedExpenses = 0;
  double _currentExpenses = 0;

  @override
  void initState() {
    super.initState();
    _headerValue = widget.productItem.headerValue;
    _plannedExpenses = widget.productItem.plannedExpenses;
    _currentExpenses = widget.productItem.currentExpenses;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Product Item'),
      ),
      body: Form(
        key: _formKey,
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              TextFormField(
                initialValue: _headerValue,
                decoration: InputDecoration(labelText: 'Header Value'),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter a header value';
                  }
                  return null;
                },
                onSaved: (value) {
                  _headerValue = value!;
                },
              ),
              TextFormField(
                initialValue: _plannedExpenses.toString(),
                decoration: InputDecoration(labelText: 'Planned Expenses'),
                keyboardType: TextInputType.number,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter a planned expenses value';
                  }
                  if (double.tryParse(value) == null) {
                    return 'Please enter a valid number';
                  }
                  return null;
                },
                onSaved: (value) {
                  _plannedExpenses = double.parse(value!);
                },
              ),
              TextFormField(
                initialValue: _currentExpenses.toString(),
                decoration: InputDecoration(labelText: 'Current Expenses'),
                keyboardType: TextInputType.number,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter a current expenses value';
                  }
                  if (double.tryParse(value) == null) {
                    return 'Please enter a valid number';
                  }
                  return null;
                },
                onSaved: (value) {
                  _currentExpenses = double.parse(value!);
                },
              ),
              ElevatedButton(
                onPressed: _saveForm,
                child: Text('Save'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _saveForm() {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      ProductItem updatedProductItem = ProductItem(
        headerValue: _headerValue,
        plannedExpenses: _plannedExpenses,
        currentExpenses: _currentExpenses,
      );
      Navigator.pop(context, updatedProductItem);
    }
  }
}