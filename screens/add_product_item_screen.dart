import 'package:flutter/material.dart';
import 'package:budget_test/models/product_item.dart';

class AddProductItemScreen extends StatefulWidget {
  @override
  _AddProductItemScreenState createState() => _AddProductItemScreenState();
}

class _AddProductItemScreenState extends State<AddProductItemScreen> {
  final _formKey = GlobalKey<FormState>();
  String _headerValue = '';
  double _plannedExpenses = 0;
  double _currentExpenses = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add ProductItem'),
      ),
      body: Form(
        key: _formKey,
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              TextFormField(
                decoration: InputDecoration(labelText: 'Header Value'),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter a header value';
                  }
                  return null;
                },
                onSaved: (value) {
                  _headerValue = value!;
                },
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Planned Expenses'),
                keyboardType: TextInputType.number,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter a planned expenses value';
                  }
                  if (double.tryParse(value) == null) {
                    return 'Please enter a valid number';
                  }
                  return null;
                },
                onSaved: (value) {
                  _plannedExpenses = double.parse(value!);
                },
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Current Expenses'),
                keyboardType: TextInputType.number,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter a current expenses value';
                  }
                  if (double.tryParse(value) == null) {
                    return 'Please enter a valid number';
                  }
                  return null;
                },
                onSaved: (value) {
                  _currentExpenses = double.parse(value!);
                },
              ),
              ElevatedButton(
                onPressed: _saveForm,
                child: Text('Save'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _saveForm() {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      Navigator.pop(context, ProductItem(
        headerValue: _headerValue,
        plannedExpenses: _plannedExpenses,
        currentExpenses: _currentExpenses,
      ));
    }
  }
}