import 'package:flutter/material.dart';
import 'package:budget_test/models/panel_item.dart';
import 'package:budget_test/models/product_item.dart';
import 'package:budget_test/widgets/panel_item_widget.dart';
import 'package:budget_test/screens/add_panel_item_screen.dart';
import 'package:budget_test/screens/add_product_item_screen.dart';
import 'package:budget_test/screens/edit_product_item_screen.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late List<PanelItem> _data;
  PanelItem? _selectedBudget;
  double _totalPlannedBudget = 0;
  double _totalCurrentBudget = 0;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Monthly Budget App'),
        actions: [
          IconButton(
            icon: Icon(Icons.add),
            onPressed: _navigateToAddItem,
          ),
        ],
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView(
              children: _data.map<Widget>((PanelItem budget) {
                return PanelItemWidget(
                  budget: budget,
                  onExpansionChange: (isExpanded) {
                    _handleExpansionChange(isExpanded, budget);
                  },
                  onAddSubItem: () async {
                    final newSubItem = await _navigateToAddProductItem(budget);  // Здесь мы передаем текущий budget
                    if (newSubItem != null) {
                      // добавьте новый подпункт в текущий элемент бюджета и обновите его
                      setState(() {
                        budget.subItems.add(newSubItem);
                        budget.plannedExpenses += newSubItem.plannedExpenses;
                        budget.currentExpenses += newSubItem.currentExpenses;
                      });
                    }
                  },
                  onEditSubItem: (int index) async { // Добавьте новый колбэк для редактирования подпункта
                    final updatedSubItem = await _navigateToEditProductItem(budget, index);
                    if (updatedSubItem != null) {
                      setState(() {
                        budget.subItems[index] = updatedSubItem;
                      });
                    }
                  },
                );
              }).toList(),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Column(
              children: [
                Text('Total planned budget: $_totalPlannedBudget'),
                Text('Total current budget: $_totalCurrentBudget'),
                Text('Difference: ${_totalPlannedBudget - _totalCurrentBudget}'),
                if (_selectedBudget != null)
                  Text('Selected budget item: ${_selectedBudget!.headerValue} with current budget: ${_selectedBudget!.currentExpenses}')
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _data = PanelItem.generateItems(5);
    _totalPlannedBudget = _data.fold(0, (previousValue, item) => previousValue + item.plannedExpenses);
    _totalCurrentBudget = _data.fold(0, (previousValue, item) => previousValue + item.currentExpenses);
  }

  void _handleExpansionChange(bool isExpanded, PanelItem budget) {
    setState(() {
      budget.isExpanded = isExpanded;
      _selectedBudget = isExpanded ? budget : null;
    });
  }

  void _navigateToAddItem() async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => AddPanelItemScreen()),
    );

    if (result != null) {
      setState(() {
        _data.add(result);
        _totalPlannedBudget += result.plannedExpenses;
        _totalCurrentBudget += result.currentExpenses;
      });
    }
  }

  Future<ProductItem?> _navigateToAddProductItem(PanelItem budget) async {
    final newSubItem = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => AddProductItemScreen(), // ваш экран для добавления подпункта
      ),
    );
    return newSubItem;
  }

  Future<ProductItem?> _navigateToEditProductItem(PanelItem budget, int index) async {
    final updatedSubItem = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => EditProductItemScreen(productItem: budget.subItems[index]),
      ),
    );
    return updatedSubItem;
  }
}