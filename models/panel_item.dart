import 'package:budget_test/models/product_item.dart';

class PanelItem {
  PanelItem({
    this.isExpanded = false,
    required this.expandedValue,
    required this.headerValue,
    required this.plannedExpenses,
    required this.currentExpenses,
    required this.subItems, // добавляем subItems в конструктор
  });

  bool isExpanded;
  String expandedValue;
  String headerValue;
  double plannedExpenses;
  double currentExpenses;
  List<ProductItem> subItems; // добавляем subItems в класс Budget

  static List<PanelItem> generateItems(int numberOfItems) {
    return List<PanelItem>.generate(
        numberOfItems,
            (int index) {
          var subItems = ProductItem.generateSubItems(
              3, index); // Let's add 3 subitems for each item
          double plannedExpenses = subItems.fold(
              0, (previousValue, subItem) =>
          previousValue +
              subItem.plannedExpenses);
          double currentExpenses = subItems.fold(
              0, (previousValue, subItem) =>
          previousValue +
              subItem.currentExpenses);
          return PanelItem(
            headerValue: 'Panel $index',
            expandedValue: 'This is item number $index',
            plannedExpenses: plannedExpenses,
            currentExpenses: currentExpenses,
            subItems: subItems,
          );
        },
        growable: true
    );
  }
}
