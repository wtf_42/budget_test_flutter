class ProductItem {
  String headerValue;
  double plannedExpenses;
  double currentExpenses;

  ProductItem({
    required this.headerValue,
    required this.plannedExpenses,
    required this.currentExpenses,
  });

  static List<ProductItem> generateSubItems(int numberOfSubItems, int seed) {
    return List<ProductItem>.generate(
        numberOfSubItems,
            (int index) {
          return ProductItem(
            headerValue: 'ProductItem ${index + seed}',
            plannedExpenses: 20.0 * (index + seed + 1),
            currentExpenses: 15.0 * (index + seed + 1),
          );
        },
        growable: true
    );
  }
}